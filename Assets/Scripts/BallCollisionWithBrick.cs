﻿using UnityEngine;

public class BallCollisionWithBrick : MonoBehaviour
{
    public GameObject BrickParticles;
    void OnCollisionEnter(Collision other) 
    {
        if (other.gameObject.tag == "Brick") 
        {
            Instantiate(BrickParticles, other.gameObject.transform.position, Quaternion.identity);
            GameManager.instance.ScoresUpdate();
            GameManager.instance.PlayHitSound();
            Destroy(other.gameObject);
        }
    }
}
