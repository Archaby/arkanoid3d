﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    public GameObject PaddlePrefab;
    //public GameObject BricksPrefab;
    public Text ScoresText;
    public GameObject Panel_GameOver;
    public GameObject Panel_LevelComplete;
    public GameObject[] LifeBallIcons;
    AudioSource audioSource;
    public AudioClip BounceSound;
    public AudioClip GameOverSound;
    public AudioClip HitSound;
    public AudioClip LevelCompleteSound;
    private int lifeCount = 3;
    private int scores = 0;
    private GameObject paddleWithBall;

    // Start is called before the first frame update
    void Awake() 
    {
        if (instance == null) 
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }    
    }

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        Setup();    
    }

    void Setup()
    {
        paddleWithBall = Instantiate(PaddlePrefab, transform.position, Quaternion.identity) as GameObject;   
    }

    public void CounterLife()
    {
        if (lifeCount > 1) 
        {
            LifeBallIcons[lifeCount - 1].SetActive(false);
            lifeCount -= 1;
            Destroy(paddleWithBall);
            Invoke("Setup", 1f);
        }    
        else 
        {
            audioSource.Stop();
            PlayGameOverSound();
            Panel_GameOver.SetActive(true);
        }    
    }

    public void ScoresUpdate() 
    {
        scores++;
        ScoresText.text = "Scores : " + scores.ToString();
        if (scores == 16) 
        {
            Destroy(GameObject.FindWithTag("Ball"));
            PlayLevelCompleteSound();
            Panel_LevelComplete.SetActive(true);
            Invoke("LoadSceneWithInvoke", 2f);
        }        
    }

    public void PlayHitSound() 
    {
        audioSource.PlayOneShot(HitSound);
    }

    public void PlayGameOverSound() 
    {
        audioSource.PlayOneShot(GameOverSound);
    }

    public void PlayBounceSound() 
    {
        audioSource.PlayOneShot(BounceSound);
    }

    public void PlayLevelCompleteSound() 
    {
        audioSource.Stop();
        audioSource.PlayOneShot(LevelCompleteSound);
    }

    void LoadSceneWithInvoke()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

}
