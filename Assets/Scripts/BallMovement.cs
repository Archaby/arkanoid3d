﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMovement : MonoBehaviour
{
    public float speedBall = 600f;
    Rigidbody rb;
    private bool isBallPlay = false;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();    
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space) && !isBallPlay)
        {
            isBallPlay = true;
            transform.parent = null;
            rb.isKinematic = false;
            rb.AddForce(new Vector3(speedBall, speedBall, 0));
        }        
    }

    void OnCollisionEnter(Collision other) {
        if (other.gameObject.tag == "BounceCol")
        {
            GameManager.instance.PlayBounceSound();
        }    
    }

}
