﻿using UnityEngine;

public class PaddleMovement : MonoBehaviour
{
    public float speedPaddle;
    Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();    
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void FixedUpdate() 
    {
        float posPaddleX =  transform.position.x + (Input.GetAxis ( "Horizontal" )  * speedPaddle * Time.deltaTime);
        transform.position = new Vector3 ( Mathf.Clamp (posPaddleX, -1.85f, 1.85f ), 0.7f, 0);  
    }
}
