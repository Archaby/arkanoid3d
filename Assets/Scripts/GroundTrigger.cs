﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundTrigger : MonoBehaviour
{
    void OnCollisionEnter(Collision other) {
        Destroy(other.gameObject);
        GameManager.instance.CounterLife();  
    }
}